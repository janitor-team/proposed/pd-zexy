Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: zexy
Upstream-Contact: IOhannes m zmölnig
Source: https://git.iem.at/pd/zexy/

Files: *
Copyright: 1999-2022, IOhannes m zmölnig <zmoelnig@iem.at>
License: GPL-2+

Files: src/freadln.c
 src/fwriteln.c
Copyright: 2007, Franz Zotter
License: GPL-2+

Files: src/doublepole~.c
Copyright: 1997-1999 Miller Puckette.
 2013-2022 IOhannes m zmölnig
License: GPL-2+

Files: src/matchbox.c
Copyright: 1999-2022, IOhannes m zmölnig <zmoelnig@iem.at>
 	1998-2004, The Regents of the University of California (Regents).
License: GPL-2+ and MIT

Files: src/dirac~.c
Copyright: 1999-2022, IOhannes m zmölnig <zmoelnig@iem.at>
 	2005, tim blechmann
License: GPL-2+

Files: src/sfplay.c
Copyright: 1999-2022, IOhannes m zmölnig <zmoelnig@iem.at>
 	1999, Winfried Ritsch <ritsch@iem.at>
License: GPL-2+

Files: src/absgn~.c
Copyright: 2006, Tim Blechmann
License: GPL-2+

Files: pd-lib-builder/*
Copyright: n/a
License: public-domain

Files: debian/*
Copyright: 2002, Guenter Geiger <geiger@debian.org>
	2010-2022, IOhannes m zmölnig <zmoelnig@iem.at>
 	2010, Jonas Smedegaard <dr@jones.dk>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation; either version 2, or (at your option) any
 later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General Public License
 (GPL) version 2 can be found in "/usr/share/common-licenses/GPL-2".
 .
 You should have received a copy of the GNU General Public License along
 with this program.  If not, see <http://www.gnu.org/licenses/>.

License: MIT
 Permission to use, copy, modify, distribute, and distribute modified
 versions of this software and its documentation without fee and without
 a signed licensing agreement, is hereby granted, provided that the
 above copyright notice, this paragraph and the following two paragraphs
 appear in all copies, modifications, and distributions.
 .
 IN NO EVENT SHALL REGENTS BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT,
 SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST PROFITS,
 ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
 REGENTS HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 .
 REGENTS SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 PARTICULAR PURPOSE. THE SOFTWARE AND ACCOMPANYING DOCUMENTATION, IF
 ANY, PROVIDED HEREUNDER IS PROVIDED "AS IS". REGENTS HAS NO OBLIGATION
 TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR
 MODIFICATIONS.

License: public-domain
 Written by Katja Vetter March-June 2015 for the public domain. No warranties.
